find_package(ROOT)
find_library(ROOT_TREEPLAYER_LIBRARY TreePlayer HINTS ${ROOT_LIBRARY_DIR} REQUIRED)
find_library(ROOT_TXMLENGINE_LIBRARY XMLIO HINTS ${ROOT_LIBRARY_DIR} REQUIRED)

set(subproject_name HNR_BaseSelection)

include_directories(
        "${ROOT_INCLUDE_DIRS}"
        "${PROJECT_SOURCE_DIR}/modules/${subproject_name}/include"
        ${AlpacaCore_INCLUDE_DIRS}
        "${PROJECT_SOURCE_DIR}/modules/rqlib"
)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS}")
file(GLOB SOURCES "${PROJECT_SOURCE_DIR}/modules/${subproject_name}/src/*.cxx")

#Pay attention to the main program name
add_executable(${subproject_name} ${subproject_name}Main.cxx ${SOURCES})
SET(RQ_LIBRARY "${PROJECT_SOURCE_DIR}/modules/rqlib/rqlib.so")

SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}")

target_link_libraries(
        ${subproject_name}
        ${AlpacaCore_LIBRARIES}
        ${ROOT_LIBRARIES}
        ${ROOT_TREEPLAYER_LIBRARY}
        ${ROOT_TXMLENGINE_LIBRARY}
        "${PROJECT_SOURCE_DIR}/modules/rqlib/rqlib.so"
)

install(TARGETS ${subproject_name} DESTINATION bin)
