#ifndef CutsHNR_BaseSelection_H
#define CutsHNR_BaseSelection_H

#include "EventBase.h"
#include "TPC_PMT_Positions.h"

class CutsHNR_BaseSelection  {

public:
  CutsHNR_BaseSelection(EventBase* eventBase);
  ~CutsHNR_BaseSelection();

  /**
   * @brief TODO: Remove. Not at stage to have 'common cuts'
   * @return
   */
  bool HNR_BaseSelectionCutsOK();

  /**
   * @brief Effective Field Theory Region Of Interest cut. Selects ROI based
   * upon S1 area
   * @return True if passes cut (ie is in region). Else False.
   */
  bool EFTROI();

  /**
   * @brief Remove S1Light bug from MDC3 sims.
   * TODO: Make calling this an option in the input file so code will work fine
   *       on non-MDC3 data
   * @return
   */
  bool G4BugCut();

  /**
   * @brief Select events which are in the upper 90CL of the NR band.
   * Selection is based upon S1 area and S2 area.
   * @return True if in region. Else False.
   */
  bool NRBandCut();

  /**
   * @brief
   * @return
   */
  float peakFractions();

  /**
   * @brief
   * @param s1ID
   * @return
   */
  float peakFractions(int s1ID);

  /**
   * @brief TODO: Seems like a pointless function to me. Maybe remove?
   * @return
   */
  float csize();

  /**
   * @brief Calculate cluster size. If bottom array phd = 0, then cluster size
   * is 50. Otherwise, SUM(chArea * pmt r_pos) / (bottom phd)
   * @param s1ID
   * @return cluster size
   */
  float csize(int s1ID);

  /**
   * @brief TODO: Again, pointless?
   * @return
   */
  bool gammaX();

  /**
   * @brief Basic version of gammaX cut. Based upon peakFractions, cluster size
   * and top-bottom asymmetry.
   * @param s1ID
   * @return
   */
  bool gammaX(int s1ID);
    


private:
  
  EventBase* m_event;

};

#endif
