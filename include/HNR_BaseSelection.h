#ifndef HNR_BaseSelection_H
#define HNR_BaseSelection_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsHNR_BaseSelection.h"

#include <TTreeReader.h>
#include <string>

class HNR_BaseSelection: public Analysis {

public:

  /// Constructor. Set which ROOT branches are loaded.
  HNR_BaseSelection();

  /// Destructor.
  ~HNR_BaseSelection();

  /// Called once before the event loop
  void Initialize();

  /// Called once per event. The event loop.
  void Execute();

  /// Called once after event loop
  void Finalize();

  /**
   * @brief Fills a standard set of histograms to fill.
   * Histograms;
   * 1. S1 vs log(S2) - values[0], values[1]
   * 2. R vs Drift - values[2], values[3]
   * 3. X vs Y - values[4], values[5]
   * 4. Energy - values[6]
   * 5. Top-Bottom Asymmetry vs Bottom Peak Fraction - values[7], values[8]
   * 6. Top-Bottom Asymmetry vs Cluster Size - values[8], values[9]
   * 7. Cluster Size - values[9]
   * @param path core path to histogram in ROOT file
   * @param values values to fill the histograms with.
   */
  void Fill_Histograms(std::string path, std::vector<float> values);

  /**
   * @brief Fill histogram for cut efficiencies.
   * TODO: Make this use a global variable so that new cuts can be added easier.
   * @param entry
   */
  void Cut_Efficiency(int entry);

protected:
  CutsHNR_BaseSelection* m_cutsHNR_BaseSelection;
  ConfigSvc* m_conf;
};

#endif
