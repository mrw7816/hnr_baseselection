#!/bin/bash
ALPACA_MODULE="HNR_BaseSelection"
NCores="${NCores:-30}"

for (( DD=1; DD<=9; DD++ ))
do
    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.7.0/2018040${DD}/*lzap.root -o ${ALPACA_MODULE}_WIMP_0${DD} -n ${NCores} -m ${ALPACA_MODULE}
done
for (( DD=10; DD<=30; DD++ ))
do
    python analysisScripts/RunNJobs_localDataset.py -d /global/cfs/cdirs/lz/data/MDC3/background/LZAP-4.7.0/201804${DD}/*lzap.root -o ${ALPACA_MODULE}_WIMP_${DD} -n ${NCores} -m ${ALPACA_MODULE}
done

cd run/${ALPACA_MODULE}
hadd ${ALPACA_MODULE}_WIMP.root ${ALPACA_MODULE}_WIMP_*
echo "Completed running over WIMP dataset"
rm ${ALPACA_MODULE}_WIMP_*
ls