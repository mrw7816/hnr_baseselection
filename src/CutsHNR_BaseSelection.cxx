#include "CutsHNR_BaseSelection.h"
#include "ConfigSvc.h"

CutsHNR_BaseSelection::CutsHNR_BaseSelection(EventBase *eventBase) {
  m_event = eventBase;
}

CutsHNR_BaseSelection::~CutsHNR_BaseSelection() {}

bool CutsHNR_BaseSelection::HNR_BaseSelectionCutsOK() {
  return true;
}

bool CutsHNR_BaseSelection::EFTROI() {
  // TODO: Make this a map variable for different ER bands
  /* double a =  4.01400983E+00;
   double b =  7.40897743E-03;
   double c =  -2.33495406E-05; // constants for fitting to Greg's er band
   double d =  4.76439917E-08;
   double e =  -4.62388657E-11;
   double f =  1.65892904E-14;*/
  double a = 3.69854341E+00;
  double b = 1.41243705E-01;
  double c = 2.19855171E-03; // constants for fitting to mdc3 band
  double d = -8.22302135E-07;
  float S1_corr = (*m_event->m_singleScatter)->correctedS1Area_phd;
  float s1AreaMin{ConfigSvc::Instance()->configFileVarMap["s1AreaMin"]};
  float s1AreaMax{ConfigSvc::Instance()->configFileVarMap["s1AreaMax"]};
  if (S1_corr > s1AreaMin && S1_corr < s1AreaMax &&
      log10((*m_event->m_singleScatter)->correctedS2Area_phd) <
          (a + b * log(S1_corr) + c * pow(S1_corr, 1) + d * pow(S1_corr, 2))) {
    return true;
  }
  return false;
}

bool CutsHNR_BaseSelection::NRBandCut() {
  double e = 3.61068774;
  double f = 1.73358476E-01;
  double g = 1.65334127E-04; // constants for fitting to mdc3 nr band
  double h = -5.12672611E-08;
  float S1_corr = (*m_event->m_singleScatter)->correctedS1Area_phd;
  if (log10((*m_event->m_singleScatter)->correctedS2Area_phd) <
      (e + f * log(S1_corr) + g * pow(S1_corr, 1) + h * pow(S1_corr, 2))) {
    return true;
  }
  return false;
}

float CutsHNR_BaseSelection::csize(int s1ID) {
  // TODO: Rename function
  float botArea = (*m_event->m_singleScatter)->s1BottomArea_phd;
  vector<float> chArea = (*m_event->m_tpcPulses)->chPulseArea_phd[s1ID];
  float xCen = (*m_event->m_tpcPulses)->bottomCentroidX_cm[s1ID];
  float yCen = (*m_event->m_tpcPulses)->bottomCentroidY_cm[s1ID];
  float cSize;
  if (botArea > 0) {
    for (int i = 0; i < chArea.size(); i++) {
      double bot_x = BottomPMTArrayXY[i][0];
      double bot_y = BottomPMTArrayXY[i][1];
      cSize += chArea[i] * sqrt(pow(bot_x - xCen, 2) + pow(bot_y - yCen, 2));
    }
  } else {
    return 50; // clusterSize = 50;
  }
  return cSize / botArea; // return clusterSize;
}

float CutsHNR_BaseSelection::csize() {
  //(*m_event->m_singleScatter)->s1PulseID
  return csize((*m_event->m_singleScatter)->s1PulseID);
}

float CutsHNR_BaseSelection::peakFractions(int s1ID) {
  float topArea = (*m_event->m_tpcPulses)->topArea_phd[s1ID];
  float botArea = (*m_event->m_tpcPulses)->bottomArea_phd[s1ID];
  float maxTopArea = 0;
  float maxBotArea = 0;
  float xCen = (*m_event->m_tpcPulses)->bottomCentroidX_cm[s1ID];
  float yCen = (*m_event->m_tpcPulses)->bottomCentroidY_cm[s1ID];
  vector<int> chID = (*m_event->m_tpcPulses)->chID[s1ID];
  vector<float> chArea = (*m_event->m_tpcPulses)->chPulseArea_phd[s1ID];
  vector<pair<int, float>> topArray;
  vector<pair<int, float>> botArray;

  for (int n = 0; n < chID.size(); n++) {
    if (topArea > 0) {
      if (chID[n] < 300) {
        topArray.push_back(make_pair(chID[n], chArea[n]));
      }
    }
    if (botArea > 0) {
      if (chID[n] >= 300) {
        botArray.push_back(make_pair(chID[n], chArea[n]));
      }
    }
  }
  for (int n = 0; n < topArray.size(); n++) {
    if (topArray[n].second > maxTopArea)
      maxTopArea = topArray[n].second;
  }
  for (int n = 0; n < botArray.size(); n++) {
    if (botArray[n].second > maxBotArea)
      maxBotArea = botArray[n].second;
  }

  float peakFractionTop = maxTopArea / topArea;
  float peakFractionBottom = maxBotArea / botArea;
  if (topArea = 0) {
    peakFractionTop = 0;
  }
  if (botArea = 0) {
    peakFractionBottom = 0;
  }
  return peakFractionBottom;
}

float CutsHNR_BaseSelection::peakFractions() {
  return peakFractions((*m_event->m_singleScatter)->s1PulseID);
}

bool CutsHNR_BaseSelection::gammaX(int s1ID) {
  float peakFractionBottom = peakFractions();
  float clusterSize = csize();
  float tba = (*m_event->m_tpcPulses)->topBottomAsymmetry[s1ID];
  double m = 0.00786722;
  double b = -1.08158152;
  if (tba < (m * clusterSize + b) && (tba < -0.7 && peakFractionBottom < 0.3)) {
    return false;
  }
  return true;
}

bool CutsHNR_BaseSelection::gammaX() {
  return gammaX((*m_event->m_singleScatter)->s1PulseID);
}

bool CutsHNR_BaseSelection::G4BugCut() {
  float S1c = (*m_event->m_singleScatter)->correctedS1Area_phd;
  float logS2c = log10((*m_event->m_singleScatter)->correctedS2Area_phd);
  if ((S1c > 492. and S1c <= 603. and logS2c < 0.001419 * S1c + 4.465 and
       logS2c > -0.00240 * S1c + 6.3257) or
      (S1c > 603. and S1c <= 835. and logS2c < 0.001419 * S1c + 4.465 and
       logS2c > 0.000933 * S1c + 4.312) or
      (S1c > 835. and S1c <= 899. and logS2c < -0.001204 * S1c + 6.656 and
       logS2c > 0.000933 * S1c + 4.312) or
      (S1c > 899. and S1c < 972. and logS2c < -0.001204 * S1c + 6.656 and
       logS2c > 0.004609 * S1c + 1.00762)) {
    return false;
  }
  return true;
}