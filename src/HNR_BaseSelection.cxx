#include "HNR_BaseSelection.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsHNR_BaseSelection.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

// SparseSvc* EFTEvents;

HNR_BaseSelection::HNR_BaseSelection() : Analysis() {

  // Load in the Single Scatters Branch
  m_event->IncludeBranch("ss");
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("pulsesSkin");
  m_event->IncludeBranch("pulsesTPC");
  m_event->Initialize();

  // Setup logging
  logging::set_program_name("HNR_BaseSelection Analysis");

  // Setup the analysis specific cuts.
  m_cutsHNR_BaseSelection = new CutsHNR_BaseSelection(m_event);

  // m_skim = new SkimSvc();
  m_conf = ConfigSvc::Instance();
  // EFTEvents = new SparseSvc();
}

HNR_BaseSelection::~HNR_BaseSelection() {
  delete m_cutsHNR_BaseSelection;
  // delete EFTEvents;
  // delete m_skim;
}

void HNR_BaseSelection::Initialize() {
  INFO("Initializing HNR_BaseSelection Analysis");
  m_cuts->mdc3()->Initialize();
}

void HNR_BaseSelection::Execute() {

  m_cuts->mdc3()->InitializeETrainVeto();

  if (m_cuts->mdc3()->SingleScatter()) {
    int cut_efficiency{0};
    Cut_Efficiency(cut_efficiency);

    int s1id{(*m_event->m_singleScatter)->s1PulseID};
    bool isItSalt{m_cuts->mdc3()->IsSalt()};
    float s1_corr{(*m_event->m_singleScatter)->correctedS1Area_phd};
    float s2_corr{(*m_event->m_singleScatter)->correctedS2Area_phd};
    float drift{(*m_event->m_singleScatter)->driftTime_ns / 1000.};
    float logS2{log10(s2_corr)};
    float x_pos{(*m_event->m_singleScatter)->correctedX_cm};
    float y_pos{(*m_event->m_singleScatter)->correctedY_cm};
    double radius{(pow(x_pos, 2) + pow(y_pos, 2))};
    double energy{0.0137 * (s1_corr / 0.15 + s2_corr / 116)};
    float csize = m_cutsHNR_BaseSelection->csize();
    float peakfracbot = m_cutsHNR_BaseSelection->peakFractions();
    float tba = (*m_event->m_tpcPulses)->topBottomAsymmetry[s1id];
    std::vector<float> histogram_values;

    histogram_values.push_back(s1_corr);
    histogram_values.push_back(logS2);
    histogram_values.push_back(radius);
    histogram_values.push_back(drift);
    histogram_values.push_back(x_pos);
    histogram_values.push_back(y_pos);
    histogram_values.push_back(energy);
    histogram_values.push_back(peakfracbot);
    histogram_values.push_back(tba);
    histogram_values.push_back(csize);
    Fill_Histograms("SS", histogram_values);

    if (!m_cuts->mdc3()->ETrainVeto()) {
      cut_efficiency += 1;
      Cut_Efficiency(cut_efficiency);
      Fill_Histograms("SS/ET", histogram_values);
      if (m_cutsHNR_BaseSelection->EFTROI()) {
        cut_efficiency += 1;
        Cut_Efficiency(cut_efficiency);
        Fill_Histograms("SS/ET/EFTROI", histogram_values);
        bool skinVeto{m_cuts->mdc3()->SkinVeto()};
        bool odVeto{m_cuts->mdc3()->ODVeto()};
        if (!skinVeto && !odVeto) {
          Fill_Histograms("SS/ET/EFTROI/Veto", histogram_values);
        }
        if (m_cutsHNR_BaseSelection->G4BugCut()) {
          cut_efficiency += 1;
          Cut_Efficiency(cut_efficiency);
          Fill_Histograms("SS/ET/EFTROI/G4LightBug/", histogram_values);
          if (m_cuts->mdc3()->Fiducial()) {
            cut_efficiency += 1;
            Cut_Efficiency(cut_efficiency);
            Fill_Histograms("SS/ET/EFTROI/G4LightBug/FV", histogram_values);
            if (!skinVeto && !odVeto) {
              cut_efficiency += 1;
              Cut_Efficiency(cut_efficiency);
              Fill_Histograms("SS/ET/EFTROI/G4LightBug/FV/Veto",
                              histogram_values);
              if (!isItSalt) {
                Fill_Histograms("SS/ET/EFTROI/G4LightBug/FV/Veto/Unsalted",
                                histogram_values);
              }
              if (m_cutsHNR_BaseSelection->NRBandCut()) {
                cut_efficiency += 1;
                Cut_Efficiency(cut_efficiency);
                Fill_Histograms("SS/ET/EFTROI/G4LightBug/FV/Veto/NRBand",
                                histogram_values);
                if (!isItSalt) {
                  cut_efficiency += 1;
                  Cut_Efficiency(cut_efficiency);
                  Fill_Histograms(
                      "SS/ET/EFTROI/G4LightBug/FV/Veto/NRBand/Unsalted",
                      histogram_values);
                  if (m_cutsHNR_BaseSelection->gammaX()) {
                    cut_efficiency += 1;
                    Cut_Efficiency(cut_efficiency);
                    Fill_Histograms("SS/ET/EFTROI/G4LightBug/FV/Veto/NRBand/"
                                    "Unsalted/gammaX",
                                    histogram_values);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void HNR_BaseSelection::Finalize() {
  INFO("Finalizing HNR_BaseSelection Analysis");

  m_hists->DrawERNRBands("SS/S1_vs_logS2", "log(s2)");
  m_hists->DrawERNRBands("SS/ET/S1_vs_logS2", "log(s2)");
  m_hists->DrawERNRBands("SS/ET/EFTROI/S1_vs_logS2", "log(s2)");
  m_hists->DrawERNRBands("SS/ET/EFTROI/G4LightBug/S1_vs_logS2", "log(s2)");
  m_hists->DrawERNRBands("SS/ET/EFTROI/G4LightBug/FV/S1_vs_logS2", "log(s2)");
  m_hists->DrawERNRBands("SS/ET/EFTROI/G4LightBug/FV/Veto/S1_vs_logS2",
                         "log(s2)");
  m_hists->DrawERNRBands("SS/ET/EFTROI/G4LightBug/FV/Veto/NRBand/S1_vs_logS2",
                         "log(s2)");
  m_hists->DrawERNRBands(
      "SS/ET/EFTROI/G4LightBug/FV/Veto/NRBand/Unsalted/S1_vs_logS2", "log(s2)");
  m_hists->DrawERNRBands(
      "SS/ET/EFTROI/G4LightBug/FV/Veto/NRBand/Unsalted/gammaX/S1_vs_logS2",
      "log(s2)");
}

void HNR_BaseSelection::Fill_Histograms(std::string path,
                                        std::vector<float> values) {
  m_hists->BookFillHist(path + "/S1_vs_logS2", 5000, 0, 1000, 5000, 1, 9,
                        values[0], values[1]);
  m_hists->BookFillHist(path + "/R_vs_Drift", 5400, 0., 5400, 500, 0., 1000.,
                    values[2], values[3]);
  m_hists->BookFillHist(path + "/X_vs_Y", 800, -80, 80, 800, -80, 80, values[4],
                    values[5]);
  m_hists->BookFillHist(path + "/Energy_ER(KeV)", 150., 0., 500., values[6]);
  m_hists->BookFillHist(path + "/tba_bpf", 150, -0.5, 1.0, 120, -1.0, 0.2,
                    values[7], values[8]);
  m_hists->BookFillHist(path + "/tba_csize", 1000, 0, 1000, 120, -1.0, 0.2,
                    values[9], values[8]);
  m_hists->BookFillHist(path + "/csize", 300., 0., 300., values[9]);
}

void HNR_BaseSelection::Cut_Efficiency(int entry) {
  m_hists->BookFillHist("Cut_Efficiency", 9., 0., 9., entry);
}